//
//  MapperTests.swift
//  MapperTests
//
//  Created by Adam Knight on 5/20/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import XCTest
@testable import Mapper

class MapperTests: XCTestCase {
	let locations: [Location] = [
		Location(name: "Apple Cupertino", userLocation: "1 Infinite Loop, Cupertino, CA"),
		Location(name: "Apple Austin RV", userLocation: "12535 Riata Vista Cir, Austin, TX"),
		Location(name: "Apple Austin PL", userLocation: "5501 W Parmer Ln, Austin, TX"),
		Location(name: "Apple Domain Northside", userLocation: "3121 Palm Way, Austin, TX"),
		Location(name: "Apple Barton Creek", userLocation: "2901 S Capitol of Texas Hwy, Austin, TX")
	]
	
	let badLocations: [Location] = [
		Location(name: "Bad Location 1", userLocation: "xyzzy"),
		Location(name: "Bad Location 2", userLocation: "101 South Equator, West Pole, Nowhere"),
		Location(name: "Bad Location 3", userLocation: "101 North Equator, East Pole, Somewhere")
	]

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLocationLookup() {
		var expectations: [XCTestExpectation] = []
		
		for location in locations {
			let lookupExpectation = expectation(description: location.name)
			expectations.append(lookupExpectation)
			
			location.lookupMissingData { (result) in
				switch result {
				case .failure(let error):
					XCTFail("Error: \(error)")
				case .success(_):
					break
				}
				lookupExpectation.fulfill()
			}
		}
		
		wait(for: expectations, timeout: 30.0)
		
//		debugPrint(locations)
    }

	func testBadLocationLookup() {
		var expectations: [XCTestExpectation] = []
		
		for location in badLocations {
			let lookupExpectation = expectation(description: location.name)
			expectations.append(lookupExpectation)
			
			location.lookupMissingData { (result) in
				switch result {
				case .failure(let error):
					switch error {
					case .geocodingError(_):
						break
					case .lookupFailure:
						fallthrough
					@unknown default:
						XCTFail("Unexpected error type: \(error)")
					}
				case .success(_):
					XCTFail("Somehow, this was found: \(location.userLocation)")
				}
				lookupExpectation.fulfill()
			}
		}
		
		wait(for: expectations, timeout: 30.0)
		
		//		debugPrint(locations)
	}
	
	func testFavoritesAddRemove() {
		let manager = FavoriteLocationsManager.shared
		let location = locations.first!
		
		manager.reset()
		
		XCTAssertTrue(manager.add(location: location))
		XCTAssertTrue(manager.locations.contains(location))
		
		XCTAssertTrue(manager.remove(location: location))
		XCTAssertFalse(manager.locations.contains(location))
	}
}
