//
//  FirstViewController.swift
//  Mapper
//
//  Created by Adam Knight on 5/20/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit
import MapKit
import Contacts


class MapViewController: UIViewController, MKMapViewDelegate {
	@IBOutlet private weak var mapView: MKMapView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		mapView.register(MKPinAnnotationView.self, forAnnotationViewWithReuseIdentifier: "location")
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		mapView.removeAnnotations(mapView.annotations)
		mapView.addAnnotations(FavoriteLocationsManager.shared.locations)
		mapView.showAnnotations(mapView.annotations, animated: true)
	}
	
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		guard let annotation = annotation as? Location else { return nil }
		
		let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "location", for: annotation) as! MKPinAnnotationView
		
		let detailLabel = UILabel(frame: .zero)
		detailLabel.text = annotation.userLocation
		
		let button = UIButton(frame: .zero)
		button.setImage(UIImage(imageLiteralResourceName: "share"), for: .normal)
		button.sizeToFit()
		
		annotationView.canShowCallout = true
		annotationView.detailCalloutAccessoryView = detailLabel
		annotationView.rightCalloutAccessoryView = button
		
		return annotationView
	}
	
	func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
		debugPrint("accessory tapped")
		
		var shareItems: [Any] = []
		
		guard let annotation = view.annotation as? Location else {
			debugPrint("Failed to fetch map annotation.")
			return
		}
		
		shareItems.append("\(annotation.name)\n\(annotation.userLocation)")
		if let url = vCardFileURL(for: annotation) {
			shareItems.append(url)
		}
		
		let vc = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
		vc.popoverPresentationController?.sourceView = view
		vc.popoverPresentationController?.sourceRect = view.frame
		self.present(vc, animated: true, completion: nil)
	}
	
	/// Creates a temporary VCF file for sharing so that Messages will show a map preview.
	///
	/// - Parameter annotation: The map annotation to create the VCF from.
	/// - Returns: A URL to a file in the user's Caches directory suitable for sharing.
	private func vCardFileURL(for annotation: Location) -> URL? {
		let coordinate = annotation.coordinate
		let urlString = "http://maps.apple.com/?ll=\(coordinate.latitude),\(coordinate.longitude)"
		
		guard let address = annotation.placemark?.postalAddress else {
			debugPrint("Failed to fetch address from placemark")
			return nil
		}
		
		let postalAddress = CNLabeledValue(label: CNLabelWork, value: address)
		let urlAddressContact = CNLabeledValue(label: "map url", value: urlString as NSString)
		
		let contact = CNMutableContact()
		contact.contactType = .organization
		contact.organizationName = annotation.name
		contact.postalAddresses = [postalAddress]
		contact.urlAddresses = [urlAddressContact]
		
		guard let dir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else { return nil }
		
		if let contactData = try? CNContactVCardSerialization.data(with: [contact]) {
			let destination = dir.appendingPathComponent("\(annotation.name).loc.vcf")
			
			do {
				try contactData.write(to: destination)
			} catch {
				debugPrint("Failed to write VCF: \(error)")
				return nil
			}
			
			return destination
		}
		
		return nil
	}
}
