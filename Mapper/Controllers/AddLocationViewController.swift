//
//  AddLocationViewController.swift
//  Mapper
//
//  Created by Adam Knight on 5/21/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit

class AddLocationViewController: UIViewController {
	
	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var addressField: UITextField!
	@IBOutlet weak var resultLabel: UILabel!
	
	var location: Location?
	
	override func viewDidLoad() {
        super.viewDidLoad()

		let leftItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButton))
		let rightItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButton))
		
		self.navigationItem.leftBarButtonItem = leftItem
		self.navigationItem.rightBarButtonItem = rightItem
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.nameField?.text = ""
		self.addressField?.text = ""
		self.resultLabel?.text = ""
	}
	
	@objc func cancelButton(sender: AnyObject) {
		self.dismiss(animated: true)
	}
	
	/// This is set by `saveButton()` and used by `lookupLocation()` to know if save should be called again after
	/// the lookup returns.  There are likely better ways — probably involving a nesting doll approach with blocks.
	var shouldSave = false
	
	/// Saves the location if it's been geocoded, otherwise starts the geocoding process.
	///
	/// - Parameter sender: The UI element that called the method.
	@objc func saveButton(sender: AnyObject) {
		if let location = self.location, location.placemark != nil {
			FavoriteLocationsManager.shared.add(location: location)
			FavoriteLocationsManager.shared.save()
			self.dismiss(animated: true)
		} else {
			shouldSave = true
			lookupLocation()
		}
	}

	/// Basic lock to prevent looking up locations multiple times at once.
	var isLookingUp = false
	
	/// Performs a lookup of the user-entered address and saves the location if `shouldSave` is `true`.
	func lookupLocation() {
		if let name = nameField?.text, let address = addressField?.text {
			let location = Location(name: name, userLocation: address)
			self.location = location
			
			guard isLookingUp == false else { return }
			isLookingUp = true
			
			location.lookupMissingData { (result) in
				switch result {
				case .failure(let error):
					debugPrint("\(error)")
					self.resultLabel?.text = NSLocalizedString("(unknown location)", comment: "Placeholder text for a location that experienced a lookup failure")
					self.shouldSave = false
				case .success(_):
					self.resultLabel?.text = "\(location.coordinate.latitude), \(location.coordinate.longitude)"
				}
				self.isLookingUp = false
				if self.shouldSave { self.saveButton(sender: self) }
			}
		}
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddLocationViewController: UITextFieldDelegate {
	/// Handles the Next/Done buttons on the keyboard.
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == self.nameField {
			self.addressField.becomeFirstResponder()
		} else if textField == self.addressField {
			self.addressField.resignFirstResponder()
		} else {
			self.view.endEditing(true)
		}
		return true
	}
	
	/// Looks up the location when the address field loses focus.
	func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
		if textField == addressField {
			lookupLocation()
		}
	}
}
