//
//  SecondViewController.swift
//  Mapper
//
//  Created by Adam Knight on 5/20/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import UIKit

class LocationListViewController: UITableViewController {
	var locations: [Location] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let leftItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addLocation(sender:)))
		self.navigationItem.leftBarButtonItem = leftItem
		self.navigationItem.rightBarButtonItem = self.editButtonItem
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		locations = FavoriteLocationsManager.shared.locations
		tableView.reloadData()
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return locations.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "location", for: indexPath)
		let location = locations[indexPath.row]
		
		cell.textLabel?.text = location.name
		cell.detailTextLabel?.text = location.userLocation
		
		return cell
	}
	
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
	override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath)
		-> UITableViewCell.EditingStyle {
			
		return .delete
	}
	
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		FavoriteLocationsManager.shared.remove(location: locations[indexPath.row])
		
		self.locations = FavoriteLocationsManager.shared.locations
		
		tableView.deleteRows(at: [indexPath], with: .automatic)
	}
	
	@objc func addLocation(sender: AnyObject) {
		self.performSegue(withIdentifier: "addLocation", sender: sender)
	}
}

