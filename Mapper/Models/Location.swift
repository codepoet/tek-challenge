//
//  Location.swift
//  Mapper
//
//  Created by Adam Knight on 5/21/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

public class Location: NSObject, Codable {
	public private(set) var name: String
	public private(set) var userLocation: String
	public private(set) var location: CLLocation?
	public private(set) var placemark: CLPlacemark?
	
	
	public init(name: String, userLocation: String, location: CLLocation? = nil, placemark: CLPlacemark? = nil) {
		self.name = name
		self.userLocation = userLocation
		
		if let location = location {
			self.location = location
		}
		
		if let placemark = placemark {
			self.placemark = placemark
			
			if let location = placemark.location {
				self.location = location
			}
		}
	}
	
	
	/// Error results from lookupMissingData.
	///
	/// - geocodingError: An error was returned from the geocoder (associated with the case).
	/// - lookupFailure: No error was returned, but no result was, either.
	public enum LookupError: Error {
		case geocodingError(Error)
		case lookupFailure
	}
	
	/// The result of lookupMissingData.  The success value is meaningless.
	public typealias LookupResult = Result<Bool,LookupError>
	
	/// A default completion handler for lookupMissingData.  It prints errors to the console.
	///
	/// - Parameter result: The result of lookupMissingData.
	public static func defaultLookupCompletion(result: LookupResult) {
		switch result {
		case .success(_):
			break
		case .failure(let error):
			debugPrint("\(error)")
		}
	}
	
	/// Attempts to fill in the `location` and `placemark` properties using each other as sources and falls back to
	/// geocoding the `userLocation` property.
	///
	/// - Parameter completion: A block to run when the lookups are complete.  Any errors encountered will be passed
	///		back in the result.
	public func lookupMissingData(completion: @escaping (LookupResult) -> Void = Location.defaultLookupCompletion) {
		if self.location == nil, let placemark = self.placemark {
			self.location = placemark.location
			completion(LookupResult.success(true))
		}
		
		else if self.placemark == nil, let location = self.location {
			let geocoder = CLGeocoder()
			geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
				guard error == nil else {
					completion(LookupResult.failure(LookupError.geocodingError(error!)))
					return
				}
				
				if let first = placemarks?.first {
					self.placemark = first
					completion(LookupResult.success(true))
				} else {
					completion(LookupResult.failure(LookupError.lookupFailure))
				}
			}
		}
		
		else if self.location == nil, self.placemark == nil {
			let geocoder = CLGeocoder()
			geocoder.geocodeAddressString(userLocation) { (placemarks, error) in
				guard error == nil else {
					debugPrint("Failed to geocode location: \(error!); \(self.userLocation)")
					completion(LookupResult.failure(LookupError.geocodingError(error!)))
					return
				}
				
				if let first = placemarks?.first {
					self.placemark = first
					self.location = first.location
					
					let address: String
					if let name = first.name,
						let locality = first.locality,
						let administrativeArea = first.administrativeArea,
						let postalCode = first.postalCode,
						let country = first.country {
						address = "\(name), \(locality), \(administrativeArea) \(postalCode), \(country)"
					} else {
						address = self.userLocation
					}
					self.userLocation = address
					
					completion(LookupResult.success(true))
				} else {
					debugPrint("Failed to geocode location: (no error returned); \(self.userLocation)")
					completion(LookupResult.failure(LookupError.lookupFailure))
				}
			}
		}
	}
	
	// MARK: - Codable
	
	/// Codable keys for Location so that it can be persisted in Favorites.
	///
	/// - name: The user-entered name for the location.
	/// - userLocation: The user-entered address of the location (or the geocoded version).
	/// - location: A CLLocation object representing the location.
	/// - placemark: A CLPlacemark object representing the geocode results.
	enum CodingKeys: String, CodingKey {
		case name
		case userLocation
		case location
		case placemark
	}
	
	required public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		self.name = try container.decode(String.self, forKey: .name)
		self.userLocation = try container.decode(String.self, forKey: .userLocation)
		
		if let locationData = try? container.decodeIfPresent(Data.self, forKey: .location) {
			self.location = try NSKeyedUnarchiver.unarchivedObject(ofClass: CLLocation.self, from: locationData)!
		}
		
		if let placemarkData = try? container.decodeIfPresent(Data.self, forKey: .placemark) {
			self.placemark = try NSKeyedUnarchiver.unarchivedObject(ofClass: CLPlacemark.self, from: placemarkData)
		}
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		
		try container.encode(self.name, forKey: .name)
		try container.encode(self.userLocation, forKey: .userLocation)
		
		if let location = self.location {
			let locationData = try NSKeyedArchiver.archivedData(withRootObject: location, requiringSecureCoding: true)
			try container.encode(locationData, forKey: .location)
		}
		
		if let placemark = self.placemark {
			let data = try NSKeyedArchiver.archivedData(withRootObject: placemark, requiringSecureCoding: true)
			try container.encode(data, forKey: .placemark)
		}
	}
}

// MARK: - Equatable
extension Location {
	static public func == (lhs: Location, rhs: Location) -> Bool {
		return lhs.name == rhs.name && lhs.userLocation == rhs.userLocation && lhs.location == rhs.location && lhs.placemark == rhs.placemark
	}
}

// MARK: - MKAnnotation
extension Location: MKAnnotation {
	public var title: String? { return self.name }
	public var subtitle: String? { return self.userLocation }
	public var coordinate: CLLocationCoordinate2D {
		// Force unwrap is unfortunate here, but if we added this to a map without ensuring there was a location
		// then it truly is a programming error.
		return location!.coordinate
	}
}
