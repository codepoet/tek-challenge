//
//  FavoriteLocationsManager.swift
//  Mapper
//
//  Created by Adam Knight on 5/21/19.
//  Copyright © 2019 Adam Knight. All rights reserved.
//

import Foundation

class FavoriteLocationsManager {
	/// An ordered list of the user's favorite locations.
	var locations: [Location] = []
	
	static let shared = FavoriteLocationsManager()
	
	enum UserDefaultsKeys: String {
		case favorites
	}
	
	init() {
		load()
	}
	
	deinit {
		save()
	}
	
	// MARK: Persistence
	
	/// Saves the favorites list.
	func save() {
		let encoder = JSONEncoder()
		
		do {
			let data = try encoder.encode(self.locations)
			UserDefaults.standard.set(data, forKey: UserDefaultsKeys.favorites.rawValue)
		} catch {
			debugPrint("Failed to save favorites: \(error)")
		}
	}
	
	/// Loads the favorites list.
	func load() {
		let decoder = JSONDecoder()
		
		do {
			if let data = UserDefaults.standard.data(forKey: UserDefaultsKeys.favorites.rawValue) {
				self.locations = try decoder.decode([Location].self, from: data)
			} else {
				debugPrint("No favorites found.")
			}
		} catch {
			debugPrint("Failed to save favorites: \(error)")
		}
	}
	
	/// Clears all known locations (in memory and in storage).
	func reset() {
		locations = []
		UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.favorites.rawValue)
	}
	
	// MARK: Adding/Removing Uniquely
	
	/// Adds a location to the favorites list.
	///
	/// - Parameter location: The Location to add.
	/// - Returns: Returns true if the location was added or false if it was a duplicate.
	@discardableResult
	func add(location: Location) -> Bool {
		if let _ = locations.index(of: location) {
			return false
		}
		locations.append(location)
		return true
	}
	
	/// Removes a location from the favorites list.
	///
	/// - Parameter location: The Location to remove.
	/// - Returns: Returns true if the location was removed or false if it was not found in the list.
	@discardableResult
	func remove(location: Location) -> Bool {
		if let idx = locations.index(of: location) {
			locations.remove(at: idx)
			return true
		}
		return false
	}
}

// Why is this not a thing?
extension Array where Element: Equatable {
	fileprivate func index(of item: Element) -> Int? {
		return firstIndex(where: { $0 == item })
	}
}
